---
title: "Women's Group"
date: 2021-07-19
category: ["church"]
---

Our Women’s Group is called the College Avenue Women’s Society or CAWS for short.

What is our cause? To help our local community.
CAWS meets once a month on the third 	Thursday of every month at 7pm in the church library. All women are welcome to come
have fellowship and meet with a cause!

The group has selected, Ashes to Life food bank,
The Women’s shelter, our BUMP preschool and many other local causes to help our community, there will be a rummage sale this fall, please keep checking the Sunday bulletin to see where you can donate.
