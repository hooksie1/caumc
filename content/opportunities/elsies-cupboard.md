---
title: "Elsie's Cupboard"
date: 2022-10-24
category: ["church"]
---

Come and shop at our Thrift Store - Elsie’s Cupboard, located on the lower level of College Ave United Methodist Church, 345 College Ave, Beaver.   

Open every Wednesday, 9am-2pm.  New inventory each week. Lots of toys, books, household decor; clothing $1.00/bag.  

Please call the church office to confirm this ministry is open: 724-755-2893.

{{< figure src="/images/opportunities/elsies-cupboard/IMG_8775.jpg" width="300px" class="floatleft" >}} 
{{< figure src="/images/opportunities/elsies-cupboard/IMG_0602.jpg" width="300px" class="floatright" >}}
{{< figure src="/images/opportunities/elsies-cupboard/IMG_0597.jpg" width="300px" class="floatleft">}}
{{< figure src="/images/opportunities/elsies-cupboard/IMG_0592.jpg" width="300px" class="floatright" >}}