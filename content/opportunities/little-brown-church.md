---
title: "Little Brown Church"
date: 2020-06-09
image: "images/opportunities/little-brown-church.jpg"
category: ["church"]
---

**Little Brown Church** is a monthly opportunity to sing the favorite hymns of our Christian faith and share a meal together in the Parlor (the large room behind the Sanctuary).

It is every second Monday of the month at 11 AM and would enjoy gathering with you!

Invite. Bring. Join.