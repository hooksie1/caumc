---
title: "A Kid's Thing"
date: 2020-06-09
image: "images/opportunities/kidsthing.jpg"
category: ["kids"]
---

A **KIDS thing!** is a monthly activity-based opportunity for families and kids to build relationships and have fun together. This has become primarily for little kids. All kids are welcomed!

It is on our hearts to do something for kids and younger families. This is for our community and our church!

Time: **4-5 PM**

2019 Dates:

**January 5th** - Popcorn & Pajamas

**February 9th** - Super Heroes Unite

**March 8th** - Fun in the Sun

**April 5th** - Easter Egg Hunt

Invite. Bring. Join.