---
title: "AA Groups"
date: 2020-06-10
image: "images/aagroups.jpg"
---

Beaver UMC is grateful that we can be a good neighbor to our Valley by providing space for AA groups:

**Mondays at 7 PM**:

   * **AA group** As Bill Sees It in the lower Fellowship Hall

   * **AA group** Sisters of Sobriety (all women’s group) AA group in the lower Large Dining Room

   {{< note "Enter through the College Ave wooden doors." >}}

**Tuesdays at 7PM**:

   * **AA group** Beaver Men's Discussion Group in the lower Fellowship Hall

   {{< note "Enter through the College Ave wooden doors." >}}

**Wednesdays at 2PM**:

   * **NA group** Easy Does It in the lower Fellowship Hall

   {{< note "Enter through the College Ave wooden doors." >}}

**Thursdays at 10:30 AM**:

   * **AA group** Keep It Simple in the upstairs Parlor

   {{< note  "Enter through the Turnpike red doors near Beaver Supermarket." >}}

**Fridays at 7 PM**:

   * **AA group** BC Friday Night Group in the lower Fellowship Hall

   {{< note "Enter through the College Ave wooden doors." >}}

**Saturdays at 10:30 PM**:

   * Keep It Simple AA group in the lower Fellowship Hall

   {{< note "Enter through the College Ave wooden doors." >}}
