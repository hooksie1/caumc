---
title: "Looking Towards the Future with Christ"
date: 2021-05-18
image: "images/sli.jpg"
category: ["kids"]
---

As a newly merged church, we committed to contracting with a church coaching group to help us discover what kind of church the Lord is calling us to become here and now and in the future. The Board of Stewards (the church’s servant leadership board) contracted with Spiritual Leadership, Inc. (SLI) in early 2021. A 7-member vision team has begun a 12-month project with an SLI coach named Paulo Lopes. The process will help us cultivate a vision of intentional disciple-making through spiritual formation, team-based leadership, and ministry action planning. Our goal is to become a church that reflects and represents Christ to each other and our neighbors so that we can partner with the Holy Spirit in fulfilling God’s mission of making disciples of Jesus, baptizing them in the name of the Father, Son, and Holy Spirit, and teaching them to obey all that he teaches until the fullness of salvation restores all things at Jesus’ second appearing (see Matthew 28:16-20).

Learn more about SLI: https://www.spiritual-leadership.org
