---
title: "Rural King Church Week"
date: 2020-06-09
image: "images/support_image_rural_king.png"
category: ["kids"]
---


https://www.ruralking.com/churchweek

**Rural King** is supporting Christian organizations by donating 10% of each valid Rural King
receipt total after sales tax to the registered organization chosen by the customer.

**Our organization is registered!**

From March 14-27, 2021, customers can upload their Rural King receipts to
www.ruralking.com/churchweek and 10% of their receipt total after sales tax will go
to the registered Christian organization of their choice.


Instructions:

* Go to or click on this link: https://www.ruralking.com/churchweek
* Under Church Week Heading, click on Search or Organization Search
* Select State:
* Select City:
* Select: our church (You should now see a picture of our church)
* Beginning on March 14 th there should be a link for you to upload your receipt.
* All receipts dated March 14 - 27, 2021 must be uploaded by April 1, 2021.


All donations will be provided by our local Rural King.
All Rural King stores are participating.


**Please share with your friends and be sure to say Thank You to Rural King management and associates when you shop.**