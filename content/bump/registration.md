---
title: BUMP 2023-2024 Registration Form
date: 2020-06-16
---

Click here to download the [2023-2024 Registration Form](/files/bump-registration-2023-2024.pdf).

Please note that your child needs to be potty trained and meet most toilet needs independently.  He/she must be able to function independently without the need for a personal care aide within the physical limitations and boundaries of the Preschool.  BUMP does not have the resources available to accommodate situations where behavior and/or developmental delays inhibit a child's ability of function independently in the classroom.

Registration is NOT completed until after the registration payment of $25 is obtained by BUMP.  Payment is accepted in the form of a check either by mail or drop off at the church office (M-F 9:00AM to 11:30AM) at:


Beaver United Methodist Preschool
345 College Avenue
Beaver, PA 15009
