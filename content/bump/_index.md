---
title: "BUMP"
date: 2020-06-15
heading: "The **B**eaver **U**nited **M**ethodist **P**reschool..."
description: "was established in 1975 to provide a stimulating and nurturing Christain environment in which to promote the development of children socially, emotionally, physically, intellectually, and spiritually. 
These goals can be accomplished with a program that offers continuity and interaction between children, teachers, parents, and the community.  We hope to give each child the opportunity to learn while having fun!"
programs: "
#### Three Day Program


4-5 Year Olds


Must be 4 years old by September 1st


Must be potty trained


Monday, Wednesday, and Friday


Morning Class: 9:00 AM to 1:00 PM


Maximum Class Size: 16 Students


##### Tuition

Registration Fee (non-refundable): $25


Tuition:  One Payment of $1260 (Nine Payments of $140)


#### Two Day Program


3-4 Year Olds


Must be 3 years old by September 1st


Must be potty trained


Tuesday and Thurday


Morning Class: 9:00 AM - 11:00 AM


Maximum Class Size: 12 Students


##### Tuition

Registration Fee (non-refundable): $25


Tuition:  One Payment of $720 (Nine Payments of $80)



#### Visits

Available by appointment
Call (724) 775-2893"
---





