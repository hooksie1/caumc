---
title: "Now Hiring"
date: 2023-03-14
---

#### Music Director

#### Position Summary

College Avenue United Methodist Church is looking for a music director. This position is
responsible for providing vision, leadership, and coordination for the church’s music ministry to
cultivate an engaging worship community.

**Supervision:** The music director shall report to the pastor

#### Responsibilities

* Selecting, preparing, and performing worship music (organ/piano/choir/special music/preludes/etc.) in consultation with the Pastor.
* Providing accompaniment for weekly Sunday worship, Ash Wednesday, Good Friday, and Christmas Eve services.
* Providing music for special services like weddings and funerals upon request and availability with the expectation of an honorarium.
* Rehearsing and directing the vocal choir and the bell choir September through May.
* Curating and maintaining a music library.
* Coordinate organ and piano maintenance, including tuning and repair. 

#### Requirements

* Candidates must demonstrate the ability to skillfully play piano and/or organ
* Have experience conducting choirs.
* Possess strong communication and organizational skills.
* Knowledge of vocal technique in order to develop the choir is required.
* Ability to adapt to change.
* Ability to work effectively as a part of a ministry team.

#### Compensation

Salary is $12,500 (negotiable based on experience)

#### How to Apply

Please forward resumes to College Avenue United Methodist Church, 345 College Avenue,
Beaver PA 15009 or collegeaveumcsecretary@gmail.com.

 *This job description is not intended, and should not be construed, to be an exclusive list of responsibilities, knowledge, or skills associated with this job. The job description may be varied at any time, if needed, to best accomplish the position’s goals and required tasks. ↑

 **Job Type:** Part-time

**Pay:** From $12,500.00 per year

**Benefits**:

* Paid time off

**Ability to commute/relocate:**

* Beaver, PA 15009: Reliably commute or planning to relocate before starting work
(Required)

**Work Location**: In person
