---
title: "Leadership Model"
date: 2020-06-16
---

“BOARD OF STEWARDS” or Single-Board MODEL
Following the “Church in the Mirror” study of Beaver United Methodist Church (BUMC) in 2018, Church Council explored and approved restructuring the administrative committees of the church into a single board.  Council believes this will streamline decision-making, lessen the number of business meetings required, and free up more members to carry out the ministries of the church.  The single-board model (which simplifies the Church Council, Trustees Committee, Finance Committee and Staff Parish Relations Committee into one unified administrative board) will be called the Board of Stewards.

On September 19, 2019, our District Superintendent Rev. Dr. Eric Park led us through time of conferencing at our annual Church Conference. Among other annual items, our Church Conference approved the adoption of the Board of Stewards administrative structure. It will be effective on January 1, 2020.

Prayerfully,

Pastor Tim Goodman	

&  Church Council Chair Carmen Romeo

--------------------------------------------------
**Beaver United Methodist Church
Proposed Board of Stewards** - Proposed Rules of Governance


### INTRODUCTION

**Section I.01** To more effectively fulfill our mission of making disciples for Jesus Christ and to better manage the temporal affairs of the church, College Avenue United Methodist Church (BUMC) shall be organized into a single governing Board of Stewards, in accordance with ¶244 and ¶247.2 of the 2016 United Methodist Church (UMC) Book of Discipline.

**Section I.02** Commencing on January 1, 2020, the functions of the former administrative committees, i.e. Church Council, Finance, Staff Parish Relations, and Trustees, will be combined into a single board, which shall be called the Board of Stewards (also referred to herein as the Board). The Board of Stewards, in accordance with the UMC Book of Discipline, will carry out all functions of these former committees. 

**Section I.03** There is no division of role or responsibilities when it comes to decision-making by the Board of Stewards. All voting members of the Board act as: the Finance Committee when financial matters are considered; the Staff-Parish Relations Committee when staff issues are considered; and the Trustees Committee when real assets or property issues are considered.  The only exception is that the Pastor shall not vote on matters pertaining to his or her own appointment, employment, or compensation.

**Section I.04** The Committee on Nominations and Leadership Development (Nomination Committee) will remain in place and function as outlined in ¶ 258.1 of the UMC Book of Discipline, as well as other ministry teams the new Board of Stewards may deem necessary. Examples of ministry teams include building repair, hospitality, outreach, discipleship, United Methodist Women, United Methodist Men, etc.

### BOARD OF STEWARDS PURPOSE

**Section II.01** The primary purpose of the Board of Stewards (Board) is to provide directional and equipping leadership as servants of Jesus Christ on our church’s behalf; so that our church can more deeply live out its mission: helping people know and follow Jesus for the Beaver Valley’s good through our core discipleship process of Equipping, Gathering, Growing, and Going. The kind of equipping leadership we are referring to comes from the image of the first disciples “mending” or “equipping” their fishing nets for future effectiveness (see Matthew 4:21) The same root word is later used to describe the church’s leadership role as “equipping” the church for effective ministry and becoming more like Christ (see Ephesians 4:12). The kind of directional leadership we are referring to will come through collaboration with the congregation and pastor for the purpose of determining that our organizational and ministry decisions are aligned with our mission, direction, and the changing contextual needs to offer effective ministry (see Acts 2:12-26, Acts 4:23-31, Acts 6:1-7). 

**Section II.02** Ministry teams (such as building repair, hospitality, outreach, discipleship, United Methodist Women, United Methodist Men, etc.) will organize and lead the internal and external ministry activities of College Avenue UMC.  It shall be the purpose of the Board to support and empower the Ministry leadership teams in carrying out the ministry activities of the church. In doing so, however, it is the express obligation and duty of the Board to make all final decisions relating to annual budgets, financial controls, audits, legal liabilities and administrative policies, and use of church facilities. The ministry teams shall report to the Board in all temporal matters, and the Board shall support and empower the ministry teams.

**Section II.03** As indicated in Section 3.07, the Lay Leader of the Church will be a voting participant in the Board of Stewards, and shall act as a liaison between the ministry teams and the Board of Stewards, to be a champion of the ministries of College Avenue UMC to the Board of Stewards.

### BOARD OF STEWARDS ORGANIZATION

**Section III.01** The Board of Stewards will have 13 members. The Pastor, Church Accountant, and Lay Leader will be standing members of the Board.  The Chairperson and nine additional members shall be elected by the Annual Church Conference upon nomination by the Nomination Committee. The initial Chairperson shall serve as Chairperson for a term of three years.  This will ensure continuity of decisions and process.

**Section III.02** The remaining initial Board of Stewards members shall be nominated to serve in three classes, starting January 1, 2020.

* One class of three persons shall be elected for one year,

* One class of three persons shall be elected for two years, and

* One class of three persons shall be elected for three years.

Three members-at-large from the former College Hill United Methodist Church shall be nominated to serve for 2021 and will fill the open class positions in 2022.

**Section III.03** Each class is to consist of one trustee, one finance and one personnel representative. Nominees should have some experience in the area of administration for which they are nominated and all should have a heart for service.  No two related people may represent the same core area (trustees, finance, or personnel).

**Section III.04** Board members may serve no more than two consecutive terms on the Board. They shall be eligible to serve again as a member of the Board after a one-year break in service.

**Section III.05** In the event of a vacancy on the Board of Stewards between church conferences, the Board of Stewards, on the recommendation of the Nominations Committee, may elect a successor to serve out the unexpired term, provided that the District Superintendent has authorized such action. In the event that a Board of Stewards member is unable to fulfill his/her term and resigns from the Board, the Nomination Committee shall appoint, by majority vote, an eligible church member to serve the remainder of the ex-member’s term.

**Section III.06** Attendance at the Board of Stewards Meetings is required. If a Board member is unable to attend, advance notice shall be submitted to the Chairperson or Pastor, and will be entered into the minutes of the Board of Stewards meeting.  The Chairperson and Pastor will meet with Board members who miss more than thirty percent (30%) of regular called meetings in a 12-month period to discern the member’s desire and ability to serve. If appropriate, the Chairperson can recommend actions to remove a Board member by a two-thirds vote of the Board members, excluding the member proposed for removal, at a regular called meeting. After the removal of a member, the Nomination Committee will immediately initiate the process to fill the vacant seat, in accordance with the provisions pertaining to the replacement of a member who has resigned (selection by majority vote, and the replacement will be subject to the same restrictions on consecutive terms).

**Section III.07** The Pastor and Lay Leader will be voting members of the Board, but the pastor shall not vote on matters pertaining to his or her own appointment, employment, or compensation.  The Church Accountant will be a non-voting member. Board members elected by the Annual Church Conference will be voting members.  The Chairperson shall only vote when required to break a tie.

### BOARD OF STEWARDS MEETINGS 

**Section IV.01** The Board of Stewards will determine the frequency of meetings, but must meet at least quarterly. It shall meet additionally at the request of the Bishop, the District Superintendent, the Pastor, or the Chairperson. The Board shall meet only with the knowledge of the Pastor or District Superintendent. The Pastor shall be present at all meetings unless he/she voluntarily excuses him/herself. The Board may meet with the District Superintendent without the Pastor present as long as the Pastor is informed in advance of the meeting and is brought into consultation immediately thereafter, under the direction of the District Superintendent.

**Section IV.02** A quorum shall be considered at least fifty-one percent (51%) of the voting members then on the Board of Stewards (six members, if the Board is fully filled, but fewer if there are vacancies on the Board at any time). No business shall be conducted if a quorum is not present (members may be present in person, or by means of electronic communication, via teleconference or videoconference, provided such participation is sufficient for both hearing and speaking as part of any discussions). There are no voting proxies for Board of Stewards members. Voting by email shall be allowed, in which case all voting members must be included and a majority of the entire Board, all of which agree either yea or nay is required to consider a vote binding. Board of Stewards members voting by email must acknowledge the receipt of the email for the vote to be binding.

**Section IV.03** Called Meetings require a seven-day advance notice and Board of Stewards members shall be provided an agenda clearly stating the item(s) to be addressed and voted on. No votes may be taken on matters, other than those stated in the agenda. 

**Section IV.04** If the required seven-day meeting notice is not feasible, emergency meetings concerning the Pastor are permissible, but must have the written approval of the District Superintendent.  Emergency meetings for the purpose of urgent church business that cannot be reasonably delayed for a normally called meeting may be conducted when so determined by the Chairperson and the Pastor. Emergency meetings are treated as regularly called meetings for purposes of the required quorum. The Chairperson will endeavor to give the Board of Stewards as much notice as is possible, under the circumstances.

**Section IV.05** It shall be a core principle of the Board of Stewards to maintain transparency of its actions through open meetings, and through ongoing communications with the congregation of BUMC. All meetings of the Board shall be open to any member of the congregation in accordance with the UMC Book of Discipline ¶722, if appropriate respect for the business being considered and decorum are maintained during the meetings. As appropriate, the Board shall enter into executive or closed session to consider personnel matters, staff salary discussions, potential litigation, contract negotiations and other items as allowed in UMC Book of Discipline ¶722.

### BOARD OF STEWARDS POWERS AND RESPONSIBILITIES

**Section V.01** The Board of Stewards shall encompass the duties and responsibilities of the former administrative committees (Church Council, Finance, Staff Parish Relations, and Trustees). This includes but is not limited to, setting the annual church budget, staff salaries, recommending appointed clergy salaries to the Church Conference for approval, annual evaluation of the Pastor, setting staff and other church policies, ensuring an annual audit and review of the church’s finances, attending to the maintenance and repairs of church property, entering into contracts on behalf of the church, and other duties performed by the former administrative committees of Church Council, Staff Parish Relations, Finance and Trustees.

**Section V.02** The Board of Stewards may adopt additional policies and procedures as needed, as long as these policies and procedures do not conflict with this document or the UMC Book of Discipline. These new or modified policies and procedures will be announced to the congregation in a transparent manner agreed by the Board and the Pastor.

**Section V.03** The Board of Stewards may create other committees, sub-committees and task forces from time to time as needed.

**Section V.04** The Board of Stewards shall create a plan to communicate regularly its activities and decisions with the congregation at large.

**Section V.05** The Pastor shall be responsible for the management of staff, and shall be accountable to the Board of Stewards for the hiring, termination and evaluation of the staff. The Board will set specific policies and procedures for the Pastor to follow as needed.

**Section V.06** As close to the beginning of each new year as possible, the Board of Stewards will schedule a half-day training session in which the rules governing the Board, Pastor, staff, and congregation are reviewed, taught, and integrated into the Board’s future goals, expectations and policies.

**Section V.07** The Board of Stewards’ primary accountability is to God on behalf of people who need the saving grace of Christ. The Board of Stewards’ primary responsibility, through the leadership of the Pastor and staff, is to ensure that BUMC stays true to its mission and realizes its vision while functioning within its rules and guiding principles.

### NOMINATIONS TO THE BOARD OF STEWARDS 

**Section VI.01** The Nominations Committee, as described in the UMC Book of Discipline ¶258.1, shall nominate candidates to be members of the Board of Stewards. The Pastor, Church Accountant, and Lay Leader will be standing members of the Board. The Nominations Committee shall nominate a Chairperson and nine additional members for election by the Annual Church Conference.

**Section VI.02** As per Section 3.05, in the event of a vacancy on the Board of Stewards between church conferences, the Board of Stewards, on the recommendation of the Nominations Committee, may elect a successor to serve out the unexpired term, provided that the District Superintendent has authorized such action.

**Section VI.03** Since the Board of Stewards will be carrying out the responsibilities of the former administrative committees of the church, it is incumbent on the Nominations Committee to nominate candidates who possess the appropriate skill sets to carry out the duties and responsibilities of those committees.

**Section VI.04** As servant leaders of the church, members of the Board of Stewards are to be:

* Lovers of Jesus

* Lovers of our local church

* Passionate about our church offering effective ministry to our community

* Prayerfully identified by our Nominations Committee as a person called by God with the gifts and graces of a Board of Stewards role

* Internally called by God to the Board of Stewards role

* Trustworthy

**Section VI.05** Actively participating in our church’s core discipleship process of Equipping, Gathering, Growing, and Going. Therefore, the Committee on Nominations shall nominate members, such that at all times, there are at least:

* Three members who are skilled in matters of Finance,

* Three members who are skilled in matters relating to Trustees, and

* Three members who are skilled in matters relating to Staff-Parish Relations.

**Section VI.06** As indicated above, all members of the Board of Stewards are expected to act as Finance, Trustees and Staff Parish Relations, but the Board may look to those members with skills in each area, to lead sub-committees, task forces, and other groups related to these functions.

### CHURCH CONFERENCE APPROVAL

**Section VII.01** Changes to this organizing document must be approved by the District Superintendent and by a duly called Church Conference of College United Methodist Church as outlined in the UMC Book of Discipline.

Approved on this ____ day of _________, 2020 in a called session of the Church Conference of College Avenue United Methodist Church with Eric Park, Superintendent of the Butler District of the Pennsylvania Annual Conference, as the Presiding Elder.


__________________________
District Superintendent

__________________________
Secretary of Church Conference

__________________________ 
Pastor 

__________________________  
Lay Leader

### Appendix

The 2016 Book of Discipline includes the following provision on creating an alternate organizational plan for the local church:

¶247.2: the charge conference, the district superintendent, and the pastor shall organize and administer the pastoral charge and churches according to the policies and plans herein set forth. When the membership size, program scope, mission resources, or other circumstances so require the charge conference may, in consultation with and upon the approval of the district superintendent, modify the organizational plans, provided that the provisions of ¶243 are observed.

The primary tasks of the local church as outlined in the 2016 Book of Discipline:

¶243. Primary Tasks – the local church shall be organized so that it can pursue its primary task and mission in the context of its own community—reaching out and receiving with joy all who will respond; encouraging people in their relationship with God and inviting them to commitment to God’s love in Jesus Christ; providing opportunities for them to seek strengthening and growth in spiritual formation; and supporting them to live lovingly and justly in the power of the Holy Spirit as faithful disciples.

In carrying out its primary task, it shall be organized so the adequate provision is made for these basic responsibilities: (1) planning and implementing a program of nurture, outreach, and witness for persons and families within and without the congregation; (2) providing for effective pastoral and lay leadership; (3) providing for financial support, physical facilities, and legal obligations of the church; (4) utilizing the appropriate relationships and resources of the district and annual conference; (5) providing for the proper creation, maintenance, and disposition of the documentary record material of the local church; and (6) seeking inclusiveness 
