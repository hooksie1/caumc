---
title: Guidelines for Regathering
author: Tim Goodman
date: 2020-06-23
image: "images/resume-worship.jpeg"
---



### Sunday Regathering Guidelines 2.0
#### *Effective October 4, 2020 at 11 am*


{{< warning "Please follow these guidelines so that we may worship the Lord in person together during the COVID-19 pandemic." >}}

**1. Preparing to Participate**

* Our worship gathering will be different for a variety of reasons. Come prayerfully with an open heart to how the Holy Spirit will invite, usher us in, and empower us to know and respond to the Presence of Jesus.

* Come with a prayerful commitment to the health and safety of yourself and others. No one will be judged if they choose to stay home.

* If you feel ill or have a temperature, please stay home.

* We want to share in the grace of Jesus together, not our germs  

**2. Entrances & Departure**

* The three main doors into the sanctuary will be opened for entrance and exit. We will observe one of our security procedures by locking these doors 10 minutes after the start of the worship service.

* The three main doors will be open for departure at the conclusion of the worship service.

**3. Masks & Physical Distancing**

* Properly worn masks will be required, unless an undisclosed health issue is concerned, inside of the church building on Sundays. College Ave UMC will have masks available if you forget a mask or do not have one.

* We will observe physically distancing recommendations of six feet. Those sheltering together, as one family, may sit together. Pews will be marked for safe seating.

* Hand sanitizers will be available for use. We will need to refrain from handshaking and hugging for now.

**4. Name Tags & Attendance Tracking**

* We will use name tags to help our newly merged congregation get to know each other.

* We will be keeping track of attendance for the purpose of understanding who might be missing among our newly merged congregation.

* We also will be keeping track of attendance for the purpose of contact tracing during the pandemic. New guests will be invited to fill out a 3x5 card.

**5. Duration of Worship Service**

* Our 11 am worship service will be a 40-45 minute worship.

* We will explore a second worship opportunity if and when it becomes evident that we need it in order to keep the sanctuary capacity at safe level.

**6. Bulletins**

* Bulletins may be picked up by worshippers at the three main sanctuary entrances; they’ll be safely prepared and arranged.

* The monitors will also be used as a paperless option.

**7. Hospitality**

* We will not gather for coffee, tea, and refreshments before and after worship. We can still be kind to each other and notice others!

**8. Tithes & Offering**

* Offering baskets will be stationed at our three exits upon arrival and departure.

* A PayPal option is available through our church website (www.caumchurch.org)

* You can still mail your tithes and offerings into the church at 345 College Ave, Beaver, PA 15009. Our church accountant will ensure that your tithes and offerings are recorded to your financial giving statement. Financial giving statements are issued at least twice a year.

**9. Congregational Singing**

* Congregational singing will not happen, due to the fact that we push air out further when we sing (as much as 15 feet or more).

* Music may be provided through special music offerings. 

* An invitation to softly sing or "hum" along to the Doxology and a few verses of a closing hymn or song will be given.

**10. Communion**

* We will celebrate the Lord's Supper by utilizing Communion packets that can be picked up at the three entrances to the sanctuary on the 1st Sunday of the month.

**11. Childcare**

* The Nursery will be available for children. Children are always welcomed in the sanctuary.

* The use of the Nursery will be determined by each family.

**12. Restrooms**

* Restrooms will be available in the Bridal Room located beside the Parlor (the large room behind the sanctuary) and kids in the Nursery who are checked in.

**13. Online Opportunity**

* We will maintain a recorded message that will be delivered on Facebook, our church website, and email. We will explore new delivery options.

* Find us on Facebook or at www.caumchurch.com
