---
title: "About Us"
date: 2018-07-12T18:19:33+06:00
heading : "We are a “**place where no one is perfect but there is always hope**” because we are “**helping people know and follow Jesus**” for the world’s good."
description : "Our core practices are **EQUIPPING** our church with hope-filled leadership to engage in **GATHERING** as the Body of Christ on Sundays, **GROWING** more like Jesus in groups, & **GOING** with Jesus into the world for the Beaver Valley’s good.


The **easiest way into our church building** Sunday mornings is through the **large wooden doors on College Avenue** at **345 College Avenue, Beaver, PA**.


We would celebrate joining you on what is next in the grand adventure of what Jesus makes possible!


Our **Sunday School program** is at **10 AM** (excluding the summer):

* Little Kids (The Nursery)

* Older Kids (The Work)

* Adults (Lower Fellowship Hall)"

services: "



Our **Sunday 11 AM traditional worship service** is held in our sanctuary. During the Summer the worship service is at **10 AM**. We have a wide variety of people who dress in suits, jeans, khakis, shirts, and dresses. We offer a community where you can be known and contribute in vital ways.


**Childcare** for little kids is provided in the **Nursery** during worship. It is staffed by our Nursery Attendant and a volunteer. Little kids can start off in the Nursery, join the Nursery after the Children’s Moment, or stay in the sanctuary. Our Nursery staff will usher our little kids into the sanctuary so that they can participate in the Children’s Moment."


expertise_title: "Have Questions"
expertise_sectors: ["Facebook: https://www.facebook.com/PABUMC", "Call us: 724-775-2893", "[What We Believe](https://www.umc.org/en/what-we-believe?fbclid=IwAR3WFfHoAt5T2JKyVY9Q3HykARG01753h_0nSwKNEilVEkrQIlhbJy-cJQM)"]
---
