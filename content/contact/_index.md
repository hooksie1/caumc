---
title         : "Contact"
date          : 2020-06-18
heading       : "Contact <span>Us.</span>"
form_heading  : "Leave A Message For The Staff"
address       : "345 College Ave"
cityzip      : "Beaver, PA 15009"
phone         : "724-775-2893"
hours         : "M-F 9:00-11:00 AM"
email         : "collegeaveumcsecretary@gmail.com"
---

